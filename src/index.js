const fs = require('fs');
const http = require('http');
const requests = require('requests');
const path = require('path');
//console.log(__dirname);
//console.log(path.join(__dirname, "/home.html"));
const filePath = path.join(__dirname, "/home.html");
const homeFile = fs.readFileSync(filePath, "utf-8");
const port = process.env.PORT || 4008;

const replaceVal = (tempVal, orgVal) => {
    let temperature = tempVal.replace("{%tempval%}", orgVal.main.temp);
    temperature = temperature.replace("{%tempmin%}", orgVal.main.temp_min);
    temperature = temperature.replace("{%tempmax%}", orgVal.main.temp_max);
    temperature = temperature.replace("{%location%}", orgVal.name);
    temperature = temperature.replace("{%country%}", orgVal.sys.country);
    temperature = temperature.replace("{%tempstatus%}", orgVal.weather[0].main);
  
    return temperature;
  };
  
  const server = http.createServer((req, res) => {
    if (req.url == "/") {
      requests(
        `https://api.openweathermap.org/data/2.5/weather?q=jabalpur&APPID=cccd67523253b17b99b9f340249a4311`
      )
        .on("data", (chunk) => {
          const objdata = JSON.parse(chunk);
          const arrData = [objdata];
          // console.log(arrData[0].main.temp);
          const realTimeData = arrData
            .map((val) => replaceVal(homeFile, val))
            .join("");
          res.write(realTimeData);
          //console.log(realTimeData);
        })
        .on("end", (err) => { 
          if (err) return console.log("connection closed due to errors", err);
          res.end();
        });
    } else {
      res.end("File not found");
    }
  });
server.listen(port, '127.0.0.1', () =>{
   console.log(`server is listting on port ${port}`);
});